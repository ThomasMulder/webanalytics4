/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package waas4;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Thomas
 */
public class Graph {
    /* The set of Nodes.*/
    private final List<Node> nodes;
    /* The set of Edges.*/
    private final List<Edge> edges;
    
    /**
     * Constructor.
     */
    public Graph() {
        this.nodes = new ArrayList();
        this.edges = new ArrayList();
    }
    
    /**
     * Add a {@code Node node} to the set of Nodes.
     * @param node the node to add.
     */
    public void addNode(Node node) {
        if (!containsNode(node)) { // Maintain Set property of no duplicates.
            this.nodes.add(node);
        }
    }
    
    /**
     * Add an {@code Edge edge} to the set of Edges.
     * @param edge the edge to add.
     */
    public void addEdge(Edge edge) {
        this.edges.add(edge);
        // Increment in- and out degrees.
        edge.getU().setOutDegree(edge.getU().getOutDegree() + 1);
        edge.getV().setInDegree(edge.getV().getInDegree() + 1);
    }
    
    /**
     * Add an {@code Edge edge} to the set of Edges.
     * @param u the 'from' Node of the edge.
     * @param v the 'to' Node of the edge.
     */
    public void addEdge(Node u, Node v) {
        this.edges.add(new Edge(u, v));
        // Increment in- and out degrees.
        u.setOutDegree(u.getOutDegree() + 1);
        v.setInDegree(v.getInDegree() + 1);
    }
    
    /**
     * Removes a {@code Node node} and all its edges.
     * @param node the node to remove.
     */
    public void removeNode(Node node) {
        removeEdgesByNode(node);
        this.nodes.remove(node); 
    }
    
    /**
     * Removes all edges corresponding to a {@code Node node}.
     * @param node the node to remove edges for.
     */
    public void removeEdgesByNode(Node node) {
        List<Edge> aux = new ArrayList();
        for (Edge edge : this.edges) {
            if (edge.getU().getName().equals(node.getName()) || edge.getV().getName().equals(node.getName())) {
                aux.add(edge);
            }
        }
        for (Edge edge : aux) {
            this.edges.remove(edge);
        }
    }
    
    /**
     * Removes an {@code Edge edge}.
     * @param edge the edge to remove.
     */
    public void removeEdge(Edge edge) {
        // Decrement the degrees.
        edge.getU().setOutDegree(edge.getU().getOutDegree() - 1);
        edge.getV().setInDegree(edge.getV().getInDegree() - 1);
        this.edges.remove(edge);
    }
    
    /**
     * Removes {@code k} Edges uniformly at random.
     * @param k the number of edges to remove.
     */
    public void removeEdgesUniformlyAtRandom(int k) {
        if (k >= this.edges.size()) {
            throw new IllegalArgumentException("k exceeds the number of edges");
        }
        while (k > 0) {
            int random = (int) ((this.edges.size() - 1) * Math.random());
            Edge edge = this.edges.get(random);
            System.out.println("Removed edge: " + edge.toString());
            removeEdge(edge);
            k--;
        }
    }
    
    /**
     * Removes {@code k} Nodes and their edges uniformly at random.
     * @param k the number of nodes to remove.
     */
    public void removeNodesUniformlyAtRandom(int k) {
        if (k >= this.nodes.size()) {
            throw new IllegalArgumentException("k exceeds the number of nodes");
        }
        while (k > 0) {
            int random = (int) ((this.nodes.size() - 1) * Math.random());
            Node node = this.nodes.get(random);
            System.out.println("Removed node: " + node.getName());
            removeNode(node);
            k--;
        }
    }
    
    /**
     * Removes {@code k} Nodes proportional to their degree.
     * That is, the probability Pr(u) or removing any {@code Node u} equals
     * {@code u.degree / sum(v.degree)} for all nodes {@code v} in this {@code Graph}.
     * @param k the number of nodes to remove.
     */
    public void removeNodesProportionalToDegreeLinear(int k) {
        int totDegree = 0;
        for (Node node : this.nodes) {
            totDegree += node.getDegree();
        }
        List<Node> aux = new ArrayList();
        while (k > 0) {
            for (Node node : this.nodes) {
                double p = ((double) node.getDegree() / (double) totDegree);
                double r = Math.random();
                if (r < p) {
                    aux.add(node);
                    k--;
                    if (k == 0) {
                        break;
                    }
                }
            }
        }
        for (Node node : aux) {
            System.out.println("Removing node: " + node.getName() + " of degree " + node.getDegree());
            removeNode(node);
        }
    }
    
    public Node getNodeByName(String name) {
        for (Node node : this.nodes) {
            if (node.getName().equals(name)) {
                return node;
            }
        }
        return null;
    }

    /**
     * @return the nodes
     */
    public List<Node> getNodes() {
        return nodes;
    }
    
    @Override
    public String toString() {
        String result = "";
        for (Edge e : this.getEdges()) {
            result = result + e.getU().getName() + " " + e.getV().getName() + "\n";
        }
        return result;
    }

    /**
     * @return the edges
     */
    public List<Edge> getEdges() {
        return edges;
    }
    
    private boolean containsNode(Node u) {
        for (Node v : this.nodes) {
            if (v.getName().equals(u.getName())) {
                return true;
            }
        }
        return false;
    }
}
