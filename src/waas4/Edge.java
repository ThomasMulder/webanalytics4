/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package waas4;

/**
 *
 * @author Thomas
 */
public class Edge {
    /** The nodes u (from) and v (to). */
    private Node u, v;
    
    /**
     * Constructor.
     * @param u the 'from' node.
     * @param v the 'to' node.
     */
    public Edge(Node u, Node v) {
        this.u = u;
        this.v = v;
    }

    /**
     * @return the u
     */
    public Node getU() {
        return u;
    }

    /**
     * @param u the u to set
     */
    public void setU(Node u) {
        this.u = u;
    }

    /**
     * @return the v
     */
    public Node getV() {
        return v;
    }

    /**
     * @param v the v to set
     */
    public void setV(Node v) {
        this.v = v;
    }
}
