/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package waas4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thomas
 */
public class WAAS4 {
    
    public WAAS4(){}
    
    /**
     * Constructs a {@code Graph} object from the data in a file found at
     * {@code file}.
     * Assumes the given file to have the structure of the edges.txt file
     * as provided by the Web Analytics course's fourth homework assignment.
     * @param file the location of the file to read.
     * @return 
     */
    private Graph readGraph(String file) {
        Graph result = new Graph();
        Scanner scan;
        try {
            scan = new Scanner(new File(file));
            while (scan.hasNextLine()) {
                String[] labels = scan.nextLine().split(" ");
                Node u = result.getNodeByName(labels[0]);
                if (u == null) {
                    u = new Node(labels[0]);
                }
                Node v = result.getNodeByName(labels[1]);
                if (v == null) {
                    v = new Node(labels[1]);
                }
                result.addNode(u);
                result.addNode(v);
                result.addEdge(u, v);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WAAS4.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        WAAS4 app = new WAAS4();
        Graph graph = app.readGraph("C:\\Users\\Thomas\\Desktop\\PageRank\\edges.txt");
        
        /** Use the code below to remove edges or nodes in different ways and so create new datasets. */
        //graph.removeEdgesUniformlyAtRandom(1);
        //graph.removeNodesUniformlyAtRandom(1);
        //graph.removeNodesProportionalToDegreeLinear(1);
        
        System.out.println(graph.toString());
    }
    
}
