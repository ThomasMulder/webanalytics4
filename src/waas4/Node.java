/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package waas4;

/**
 *
 * @author Thomas
 */
public class Node {
    /* The name of the Node. */
    private String name;
    
    /* The in- and out degree. */
    private int inDegree, outDegree;
    
    /**
     * Constructor.
     * @param name the name of the Node.
     */
    public Node(String name) {
        this.name = name;
        this.inDegree = 0;
        this.outDegree = 0;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the inDegree
     */
    public int getInDegree() {
        return inDegree;
    }

    /**
     * @return the outDegree
     */
    public int getOutDegree() {
        return outDegree;
    }
    
    /**
     * Get the sum of the in- and out degrees.
     * @return 
     */
    public int getDegree() {
        return (inDegree + outDegree);
    }

    /**
     * @param inDegree the inDegree to set
     */
    public void setInDegree(int inDegree) {
        this.inDegree = inDegree;
    }

    /**
     * @param outDegree the outDegree to set
     */
    public void setOutDegree(int outDegree) {
        this.outDegree = outDegree;
    }
    
    @Override
    public String toString() {
        return (this.name + " of (" + inDegree + ", " + outDegree + ")");
    }
}
